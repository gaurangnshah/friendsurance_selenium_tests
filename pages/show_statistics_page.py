from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

from pages.base_page import BasePageObject, BaseList


class ShowStatistics(BasePageObject):

    def select_month(self, year_month):
        month_dropdown = Select(self.find_element(By.NAME, "month"))
        month_dropdown.select_by_visible_text(year_month)

    @property
    def list(self):
        return List(self.driver)

    @property
    def chart(self):
        return self.driver.find_elements_by_xpath("//*[name()='svg']")[0]


class List(BaseList):

    # First row contains header and last row is empty
    table_rows = (By.XPATH, "//table/tbody/tr[position() < last() and position() > 1]")

    def get_rows(self):
        return [RowWrapper(row) for row in self.driver.find_elements(*self.table_rows)]


class RowWrapper(object):

    def __init__(self, element):
        self.element = element

    @property
    def category(self):
        return self.element.find_element_by_xpath("./td[1]").text

    @property
    def value(self):
        return self.element.find_element_by_xpath("./td[2]").text
