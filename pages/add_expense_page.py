from selenium.webdriver.common.by import By

from pages.base_page import BasePageObject


class AddExpense(BasePageObject):

    day_input = lambda self: self.find_element(By.ID, "day")
    month_input = lambda self: self.find_element(By.ID, "month")
    year_input = lambda self: self.find_element(By.ID, "year")

    category_input = lambda self: self.find_element(By.ID, "category")
    amount_input = lambda self: self.find_element(By.ID, "amount")
    reason_input = lambda self: self.find_element(By.ID, "reason")
    submit_button = lambda self: self.find_element(By.ID, "submit")

    def date(self, date):
        self.day_input().clear()
        self.month_input().clear()
        self.year_input().clear()

        self.day_input().send_keys(date.split(".")[0])
        self.month_input().send_keys(date.split(".")[1])
        self.year_input().send_keys(date.split(".")[2])
        return self

    def category(self, category):
        self.category_input().send_keys(category)
        return self

    def amount(self, amount):
        self.amount_input().clear()
        self.amount_input().send_keys(amount)
        return self

    def reason(self, reason):
        self.reason_input().clear()
        self.reason_input().send_keys(reason)
        return self

    def submit(self):
        self.submit_button().click()

    def add_expense(self, date, category, amount, reason):
        self.date(date).category(category).amount(amount).reason(reason).submit()
