from selenium.webdriver.common.by import By

from pages.add_expense_page import AddExpense
from pages.base_page import BasePageObject
from pages.list_categories_page import ListCategories
from pages.list_expense_page import ListExpense
from pages.show_statistics_page import ShowStatistics


class NavigationBar(BasePageObject):

    add_expense_tab = lambda self: self.find_element(By.ID, "go_add_expense")
    list_expense_tab = lambda self: self.find_element(By.ID, "go_list_expenses")
    list_category_tab = lambda self: self.find_element(By.ID, "go_list_categories")
    show_statistics_tab = lambda self: self.find_element(By.ID, "go_show_statistics")

    def __init__(self, driver):
        self.driver = driver
        super(NavigationBar, self).__init__(driver)

    @property
    def go_to_add_expense(self):
        self.add_expense_tab().click()
        return AddExpense(self.driver)

    @property
    def go_to_list_expense(self):
        self.list_expense_tab().click()
        return ListExpense(self.driver)

    @property
    def go_to_list_categories(self):
        self.list_category_tab().click()
        return ListCategories(self.driver)

    @property
    def go_to_show_statistics(self):
        self.show_statistics_tab().click()
        return ShowStatistics(self.driver)


class HomePage(NavigationBar):

    def __init__(self, driver, base_url):
        super(HomePage, self).__init__(driver)
        self.base_url = lambda path: "%s/%s" % (base_url, path)

    def navigate_to(self, path=""):
        self.driver.get(self.base_url(path))
