from pages.base_page import BasePageObject
from selenium.webdriver.support import expected_conditions as EC


class Alert(BasePageObject):

    def __init__(self, driver):
        super(Alert, self).__init__(driver)
        self.wait(10).until(EC.alert_is_present())
        self.alert = self.driver.switch_to_alert()

    def accept(self):
        self.alert.accept()

    @property
    def text(self):
        return self.alert.text
