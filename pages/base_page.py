from itertools import islice

from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePageObject(object):

    driver = None

    def __init__(self, driver, **kwargs):
        self.driver = driver
        self.__dict__.update(kwargs)

    def wait_until_visible(self, by, locator_value, timeout=10, driver=None):
        try:
            return self.wait(timeout).until(EC.visibility_of_element_located((by, locator_value)))
        except TimeoutException:
            raise NoSuchElementException(
                msg="Element not found at {} after timeout {}".format((by, locator_value), timeout))

    def is_element_exist(self, *locator):
        try:
            return self.driver.find_element(*locator)
        except NoSuchElementException:
            return None

    def wait(self, timeout=10):
        return WebDriverWait(self.driver, timeout)

    def find_element(self, by, value):
        return self.driver.find_element(by, value)

    def find_e(self, by, value):
        return self.driver.find_element(by, value)


class BaseList(BasePageObject):

    table_rows = (By.XPATH, "//table/tbody/tr")

    def find_row(self, predicate):
        return next((row for row in self.get_rows() if predicate(row)), None)

    def get_nth(self, n):   # 1-based indexing
        def nth(iterable, n, default=None):
            """Returns the nth item or a default value"""
            return next(islice(iterable, n, None), default)

        return nth(self.get_rows(), n)

    @property
    def get_row_count(self):
        return self.get_rows().__len__()

    def delete_all(self):
        delete_links = [ele.get_attribute("href")+"&confirmed=yes" for ele in
                        self.driver.find_elements_by_css_selector("a[id^='delete']")]

        map(self.driver.get, delete_links)


def FindBy(by, value):
    def decorator(func):
        def wrapper(self):
            return self.driver.find_element(by, value)
        return wrapper
    return decorator
