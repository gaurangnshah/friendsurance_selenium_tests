from selenium.webdriver.common.by import By

from pages.base_page import BasePageObject


class Register(BasePageObject):

    username_input = lambda self: self.find_element(By.ID, "login")
    password_input = lambda self: self.find_element(By.ID, "password1")
    repeat_password_input = lambda self: self.find_element(By.ID, "password2")
    submit_button = lambda self: self.find_element(By.ID, "submit")
    alert_text = lambda self: self.find_element(By.CSS_SELECTOR, ".alert.alert-danger")

    def register(self, username, password, repeat_password):
        self.username_input().send_keys(username)
        self.password_input().send_keys(password)
        self.repeat_password_input().send_keys(repeat_password)
        self.submit_button().click()
        return self

    @property
    def error_text(self):
        return self.alert_text().text
