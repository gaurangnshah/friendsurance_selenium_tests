from itertools import islice
from selenium.webdriver.common.by import By

from pages.add_expense_page import AddExpense
from pages.alert import Alert
from pages.base_page import BasePageObject


class ListExpense(BasePageObject):

    @property
    def expenses(self):
        return self.driver.find_elements_by_xpath("//table[@class='table']//tr")

    @property
    def list(self):
        return List(self.driver)


class List(BasePageObject):

    table_rows = (By.XPATH, "//table/tbody/tr")

    def get_rows(self):
        def _row():
            return [RowWrapper(row, self.driver) for row in self.driver.find_elements(*self.table_rows)]

        yield _row()
        while self._next_page:
            self._next_page.click()
            yield _row()

    def find_row(self, predicate):
        self._go_to_first_page()
        for page in self.get_rows():
            row = next((row for row in page if predicate(row)), None)
            if row:
                return row

    def find_rows(self, predicate):
        self._go_to_first_page()
        rows = []

        for page in self.get_rows():
            rows.extend([row for row in page if predicate(row)])

        return rows

    def get_nth(self, n):   # 1-based indexing
        def nth(iterable, n, default=None):
            """Returns the nth item or a default value"""
            return next(islice(iterable, n, None), default)

        expected_page = n / 10
        index = 0
        for page in self.get_rows():
            index += 1
            if index >= expected_page:
                return nth(page, n % 10 - 1)

    @property
    def get_row_count(self):
        count = 0
        for page in self.get_rows():
            count += page.__len__()

        self._go_to_first_page()
        return count

    def _go_to_first_page(self):
        while self._previous_page:
            self._previous_page.click()

    @property
    def _next_page(self):
        return self.is_element_exist(By.CSS_SELECTOR, "img[alt='active arrow right']")

    @property
    def _previous_page(self):
        return self.is_element_exist(By.CSS_SELECTOR, "img[alt='active arrow left']")

    def delete_all(self):
        while True:
            delete_links = [ele.get_attribute("href")+"&confirmed=yes" for ele in
                            self.driver.find_elements_by_css_selector("a[id^='delete']")]

            map(self.driver.get, delete_links)
            if not self._next_page:
                break


class RowWrapper(BasePageObject):

    def __init__(self, element, driver):
        self.element = element
        super(RowWrapper, self).__init__(driver)

    @property
    def date(self):
        return self.element.find_element_by_xpath("./td[1]").text

    @property
    def category(self):
        return self.element.find_element_by_xpath("./td[2]").text

    @property
    def amount(self):
        return self.element.find_element_by_xpath("./td[3]").text

    @property
    def reason(self):
        # time.sleep(3)
        return self.element.find_element_by_xpath("./td[4]").text

    @property
    def delete(self):
        self.element.find_element_by_css_selector("a[id^='delete']").click()
        return Alert(self.driver)

    @property
    def edit(self):
        self.element.find_element_by_css_selector("a[id^='edit']").click()
        return AddExpense(self.driver)

    @property
    def copy(self):
        self.element.find_element_by_css_selector("a[id^='copy']").click()
        return self
