from selenium.webdriver.common.by import By

from pages.alert import Alert
from pages.base_page import BasePageObject, BaseList


class ListCategories(BasePageObject):

    add_category_link = lambda self: self.find_element(By.ID, "go_add_category")

    @property
    def add_category(self):
        self.add_category_link().click()
        return AddCategory(self.driver)

    @property
    def list(self):
        return List(self.driver)


class AddCategory(BasePageObject):

    name_input = lambda self: self.driver.find_element(By.ID, "name")
    submit_button = lambda self: self.driver.find_element(By.ID, "submit")

    def name(self, category):
        self.name_input().clear()
        self.name_input().send_keys(category)
        return self

    def submit(self):
        self.submit_button().click()
        return ListCategories(self.driver)


class List(BaseList):

    def get_rows(self):
        return [RowWrapper(row, self.driver) for row in self.driver.find_elements(*self.table_rows)]


class RowWrapper(BasePageObject):

    def __init__(self, element, driver):
        self.element = element
        super(RowWrapper, self).__init__(driver)

    @property
    def name(self):
        return self.element.find_element_by_xpath("./td[1]").text

    @property
    def edit(self):
        self.element.find_element_by_css_selector("a[id^='edit']").click()
        return AddCategory(self.driver)

    @property
    def delete(self):
        self.element.find_element_by_css_selector("a[id^='delete']").click()
        return Alert(self.driver)
