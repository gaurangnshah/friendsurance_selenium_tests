from selenium.webdriver.common.by import By

from pages.base_page import BasePageObject
from pages.register_page import Register


class LoginPage(BasePageObject):

    username_input = lambda self: self.find_element(By.ID, "login")
    password_input = lambda self: self.find_element(By.ID, "password")
    submit_button = lambda self: self.find_element(By.ID, "submit")
    alert_text = lambda self: self.find_element(By.CSS_SELECTOR, ".alert.alert-danger")
    register_link = lambda self: self.find_element(By.LINK_TEXT, "Register new user")

    def login(self, username, password):
        self.username_input().send_keys(username)
        self.password_input().send_keys(password)
        self.submit_button().click()
        return self

    @property
    def success(self):
        return not self.is_element_exist(By.ID, "login")

    @property
    def error_text(self):
        return self.alert_text().text

    @property
    def go_to_register_page(self):
        self.register_link().click()
        return Register(self.driver)
