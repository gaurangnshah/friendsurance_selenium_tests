
def test_edit_category(list_categories_page, add_category):

    def row(name):
        return list_categories_page.list.find_row(lambda _row: _row.name == name)

    row(add_category.get("name")).edit.name("xyz").submit()

    assert row("xyz") is not None


def test_delete_category(list_categories_page, add_category):

    def row(name):
        return list_categories_page.list.find_row(lambda _row: _row.name == name)

    row(add_category.get("name")).delete.accept()
    assert row(add_category.get("name")) is None


def test_add_category(list_categories_page, add_category):
    row = list_categories_page.list.find_row(lambda _row: _row.name == add_category.get("name"))
    assert row is not None
