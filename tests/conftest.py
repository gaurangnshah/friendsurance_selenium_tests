import pytest

from pages.home_page import HomePage
from pages.login_page import LoginPage
from utils import rand


@pytest.fixture()
def login_page(browser):
    return LoginPage(browser)


@pytest.fixture()
def register_page(login_page):
    return login_page.go_to_register_page


@pytest.fixture()
def home_page(browser, login_page, base_test_config):
    url = base_test_config['app']['url']
    username = base_test_config['app']['username']
    password = base_test_config['app']['password']

    if not login_page.login(username, password).success:
        raise Exception("Login Error: Not able to login using %s username and %s password" % (username, password))
    home = HomePage(browser, url)
    # home.navigate_to("/")
    return home


@pytest.fixture()
def add_expense_page(home_page):
    return home_page.go_to_add_expense


@pytest.fixture()
def list_expense_page(home_page):
    print "list_expense_page"
    return home_page.go_to_list_expense


@pytest.fixture()
def list_categories_page(home_page):
    return home_page.go_to_list_categories


@pytest.fixture()
def add_category(list_categories_page):
    print "add_category"
    name = rand.random_string()
    list_categories_page.add_category.name(name).submit()
    return locals()


@pytest.fixture()
def add_expense(add_expense_page, request):
    reason = rand.random_string()
    date = rand.random_date()
    amount = rand.random_amount()
    category = "demo"

    parameter = getattr(request, "param", None)
    if parameter:

        reason = parameter.get("reason") if parameter.get("reason") else reason
        date = parameter.get("date") if parameter.get("date") else date
        amount = parameter.get("amount") if parameter.get("amount") else amount
        category = parameter.get("category") if parameter.get("category") else category

    add_expense_page.date(date).amount(amount).category(category).reason(reason).submit()
    return locals().copy()
