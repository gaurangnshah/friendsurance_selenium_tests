# -*- coding: utf-8 -*-

from datetime import datetime
import time
from utils import rand


def test_add_expense(list_expense_page, add_expense):

    def _convert_date(date):
        return datetime.strptime(date, "%d.%m.%Y").strftime("%d.%m.%y")

    row = list_expense_page.list.find_row(lambda _row: _row.reason == add_expense.get("reason"))
    assert row.reason == add_expense.get("reason")
    assert float(row.amount.replace(",", ".").replace(u"\u20ac", "")) == float(add_expense.get("amount"))
    assert row.category == add_expense.get("category")
    assert row.date == _convert_date(add_expense.get("date"))


def test_delete_expense(list_expense_page, add_expense):

    def row():
        return list_expense_page.list.find_row(lambda _row: _row.reason == add_expense.get("reason"))

    row().delete.accept()
    # TODO: make change in List class to wait until row becomes stale
    time.sleep(2)

    assert row() is None


def test_edit_expense(add_category, add_expense, list_expense_page):

    def row(_reason):
        return list_expense_page.list.find_row(lambda _row: _row.reason == _reason)

    date = rand.random_date()
    reason = rand.random_string()
    amount = rand.random_amount()
    category = add_category.get("name")

    expense = row(add_expense.get("reason")).edit
    expense.add_expense(date, category, amount, reason)
    assert row(add_expense.get("reason")) is None
    assert row(reason) is not None

