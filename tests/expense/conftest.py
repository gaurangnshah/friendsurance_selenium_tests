import pytest


@pytest.fixture(scope="session", autouse=True)
def init_config():
    config = {"config_done": False}
    return config


@pytest.fixture(autouse=True)
def add_category_if_not_exist(list_categories_page, init_config):
    if not init_config["config_done"]:
        if not list_categories_page.list.find_row(lambda _row: _row.name == "demo"):
            list_categories_page.add_category.name("demo").submit()

        init_config["config_done"] = True
