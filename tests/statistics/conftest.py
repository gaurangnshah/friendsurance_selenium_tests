import pytest


@pytest.fixture(scope="session", autouse=True)
def init_config():
    config = {"config_done": False}
    return config


@pytest.fixture()
def delete_all_expenses(list_expense_page, init_config):
    if not init_config["config_done"]:
        list_expense_page.list.delete_all()


@pytest.fixture()
def delete_all_categories(list_categories_page, init_config):
    if not init_config["config_done"]:
        list_categories_page.list.delete_all()


@pytest.fixture(autouse=True)
def create_date_for_tests(delete_all_expenses, delete_all_categories, home_page, init_config):

    if not init_config["config_done"]:
        list_categories_page = home_page.go_to_list_categories
        list_categories_page.add_category.name("shopping").submit()
        list_categories_page.add_category.name("bills").submit()

        add_expense_page = home_page.go_to_add_expense
        add_expense_page.add_expense("01.01.2017", "shopping", "100.90", "shopping")

        add_expense_page = home_page.go_to_add_expense
        add_expense_page.add_expense("05.01.2017", "bills", "00.10", "small bill")

        add_expense_page = home_page.go_to_add_expense
        add_expense_page.add_expense("10.02.2017", "shopping", "10", "shopping")

        add_expense_page = home_page.go_to_add_expense
        add_expense_page.add_expense("20.02.2017", "bills", "20", "small bill")

        init_config["config_done"] = True


@pytest.fixture()
def show_statistics_page(home_page):
    return home_page.go_to_show_statistics
