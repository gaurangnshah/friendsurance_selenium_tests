# -*- coding: utf-8 -*-
import pytest


def test_chart_is_present(show_statistics_page):
    assert show_statistics_page.chart


@pytest.mark.parametrize(argnames=('date', 'category_value'),
                         argvalues=[("2017.01", {"bills": "0,10 €", "shopping": "100,90 €", "Summe": "101,00 €"}),
                                    ("2017.02", {"bills": "20,00 €", "shopping": "10,00 €", "Summe": "30,00 €"})],
                         ids=("2017.01", "2017.02"))
def test_statistics_data(show_statistics_page, date, category_value):

    def amount_of(category):
        return show_statistics_page.list.find_row(lambda _row: _row.category == category).value

    show_statistics_page.select_month(date)

    for category, amount in category_value.iteritems():
        assert amount_of(category) == amount.decode("utf-8")
