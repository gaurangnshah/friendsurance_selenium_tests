from pages.alert import Alert


def test_login_error_message(login_page):
    login_page.login("aaaa", "bbbbb")
    assert "unknown login or wrong password" == login_page.error_text


def test_register_page(register_page):
    register_page.register("abc", "dddd", "asssss")
    error_text = Alert(register_page.driver).text
    assert error_text == "Error: Passwords aren't equal!"


def test_register_duplicate_user(register_page):
    register_page.register("testuser", "password", "password")
    # TODO: change the error message
    assert register_page.error_text == "password1 wasn't equal to password2"
