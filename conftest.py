import json
import os

import pytest
import time
from selenium import webdriver

base_dir = os.path.dirname(__file__)
DEFAULT_BROWSER = "Chrome"


def pytest_addoption(parser):
    parser.addoption('--test-config', action="store", default='config/test-config.json',
                     help="Specify location test configuration file")
    parser.addoption('--base-url', action="store", default=None,
                     help="Specify Application url. Overrides url set in test config file")


@pytest.fixture(scope='session')
def base_test_config_location(request):
    return request.config.option.test_config


@pytest.fixture(scope='session')
def base_config_url(request):
    return request.config.option.base_url


def get_config_file(file_name):
    with open(os.path.join(base_dir, file_name)) as cfg_file:
        cfg = json.load(cfg_file)
    return cfg


@pytest.fixture(scope='session')
def base_test_config(base_test_config_location, base_config_url):
    conf = get_config_file(base_test_config_location)
    conf.setdefault('app', {})

    if base_config_url:
        conf['app']['url'] = base_config_url

    return conf


@pytest.fixture(scope="session")
def browser_config(base_test_config):
    return base_test_config.get("webdriver", {})


def browser_name(browser_config):
    return browser_config.get("browser", DEFAULT_BROWSER).capitalize()


def browser_factory(browser_config, request):
    browser_driver = browser_name(browser_config)
    # return webdriver.__dict__[browser_driver]()
    if browser_driver in ('Firefox',):
        return webdriver.Firefox()
    else:
        return webdriver.Chrome()


@pytest.fixture(scope="function")
def browser(request, browser_config, base_test_config):
    browser = browser_factory(browser_config, request)
    browser.get(base_test_config['app']['url'])
    request.node._browser = browser
    request.node._browser_config = browser_config

    request.addfinalizer(lambda: browser.quit())

    return browser


def _capture_screenshot(node):
    _browser = getattr(node, '_browser', None)
    file_name = None
    if browser:
        file_path = "{base_dir}/report".format(base_dir=base_dir)
        file_name = "{test_name}_{time}.png".format(test_name=node.name, time=str(int(time.time())))

        _browser.get_screenshot_as_file("{file_path}/{file_name}".format(file_path=file_path, file_name=file_name))
    return file_name


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    """
    Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
    :param item:
    :param call:
    :return: None
    """

    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when in ("call", "setup"):
        xfail = hasattr(report, 'wasxfail')
        if (report.skipped and xfail) or (report.failed and not xfail):
            file_name = _capture_screenshot(item)
            if file_name:
                html = '<div><img src="%s" alt="%s" style="width:304px;height:228px;" ' \
                       'onclick="window.open(this.src)" align="right"/></div>' % (file_name, file_name)
                extra.append(pytest_html.extras.html(html))

        report.extra = extra
