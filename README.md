# README #



### What is this repository for? ###

* To Test the personal expense application using selenium webdriver 
* URL: http://thawing-shelf-73260.herokuapp.com

### How do I get set up? ###

#### Pre-requisites ####
* Python 2.7 
* Firfox or Chrome Browser
* geckodriver or chromedriver

#### Linux ####
+ Run the create_env.sh file using following command, this will create python virtual environment and will install all 
the required dependencies
    * `source create_env.sh`
+ Download the latest version of geckoxdriver or chromedriver from following locations based on your OS.
    * geckdriver: https://github.com/mozilla/geckodriver/releases
    * chromedriver: https://sites.google.com/a/chromium.org/chromedriver/
+ set them in your **PATH** variable. 

#### Windows ####
+ Run the create_env.sh file using following command
    * `create_env.bat`
+ Download the latest version of geckoxdriver or chromedriver from following locations based on your OS.
    * geckdriver: https://github.com/mozilla/geckodriver/releases
    * chromedriver: https://sites.google.com/a/chromium.org/chromedriver/
+ set them in your **PATH** variable. 

### How to run tests ###
* make sure user with following credential exist
    * **username**: testuser
    * **password**: password
* Go to project directory and run following command 
    * `pytest`
* To Change the browser (only Firefox or Chrome) go to `config/test-config.json` and change browser name

### Rerports ###
* XUnit and HTML report will be generated in Report folder


