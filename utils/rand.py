import string
import random

from datetime import date


def random_string(length=10):
    return "".join([random.choice(string.ascii_letters) for _ in range(length)])


def random_amount(decimal=3):
    return "".join(([str(random.choice(range(0, 9))) for _ in range(decimal)])) + "." + \
           "".join(([str(random.choice(range(0, 9))) for _ in range(2)]))


def random_date():
    start_date = date.today().replace(day=1, month=1, year=2000).toordinal()
    end_date = date.today().toordinal()
    return date.fromordinal(random.randint(start_date, end_date)).strftime("%d.%m.%Y")
